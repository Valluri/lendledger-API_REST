# LendLedger
This application describes the contract between the LL platform and particular Loan Management Systems which someone wants to integrate into the LL platform. This integration is named as Blockchain Integration Layer.

This document may concern developers which want to integrate their systems into the LL platform.

## API for devlopers
The lendledger API contains information 

## Deployment
The project is hosted on GitHub. 

### Prerequisites
Make sure you have Node.js 6.9.0 or higher installed. If not, install it 

```sh
# Check your node version using this command
node --version
```
Make sure you have MongoDB installed. If not, install it ([Install MongoDB](https://docs.mongodb.com/manual/administration/install-community/) is recommended).


### Environment Setup
```sh
# Clone the project
git clone https://github.com/lendledger/api.git
cd lendledger

# Install the node_modules
npm install
```
### Development mode
For running this application, you have to specify certificate path, stellar accountId and Secret.

```sh
CERT_PATH=<path>/ ACCOUNT_ID=<accountId> ACCOUNT_SECRET=<secret> npm start
```

### Testing mode
For running this application, you have to specify certificate path, stellar accountId, Secret and TLS_flag.

```sh
CERT_PATH=<path>/ ACCOUNT_ID=<accountId> ACCOUNT_SECRET=<secret> TLS_FLAG=0 npm test
```
## License


## Credits

-