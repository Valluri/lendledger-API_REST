/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 27-July-2018
 */
var express = require('express');
var router = express.Router();
var Q = require('q');
var config = require('./../config/config.json');
var configJS = require('./../config/config');

var mongo = require('mongoskin');
var stripe = require("stripe")("sk_test_TzR08jzSL2CZhBMvRqe0q4h5");

var dbType = config.dbType;
var dbIp = config.dbIp;
var dbPort = config.dbPort;
var dbUser = config.dbUser;
var dbPass = config.dbPass;
var dbName = config.dbName;
var connectionString = "";
var db;
var StellarSdk = require('stellar-sdk');
var server;
var promise = require('bluebird');

var options = {
    // Initialization Options
    promiseLib: promise
};
var pgp = require('pg-promise')(options);
var mysql = require('mysql');
var connection;

if (dbType === "postgres") {
    connectionString = dbType + "://" + dbUser + ":" + dbPass + "@" + dbIp + ":" + dbPort + "/" + dbName + "?sslmode=disable";
    db = pgp(connectionString);
    // server = new StellarSdk.Server('https://horizon-testnet.stellar.org');
    server = new StellarSdk.Server(config.stellarServer);

} else if (dbType === "mysql") {
    connection = mysql.createConnection({
        host: dbIp,
        port: dbPort,
        user: dbUser,
        password: dbPass,
        database: dbName
    });
    server = new StellarSdk.Server(config.stellarServer);
    connection.connect(function (err) {
        if (!err) {
            // console.log(dbType + " Database is connected ... \n\n");
        } else {
            // console.log("Error connecting database ... \n\n");
        }
    });
}

var stellarNetwork = config.stellarNetwork;

if (stellarNetwork === "test") {
    StellarSdk.Network.useTestNetwork();
} else if (stellarNetwork === "public") {
    StellarSdk.Network.usePublicNetwork();
}

var request = require('request');

var service = {};

service.changeTrust = changeTrust;
service.issueAsset = issueAsset;

module.exports = service;

/**
 * @author:Akshay Misal
 * @link: POST /asset/changeTrust
 * @description: This funcation will create the trust-line between users.
 * @param {firstName} req 
 * @param {lastName} req 
 * @param {accountId} req 
 * @param {accountNo} req 
 * @param {accountIfsc} req 
 * @param {assetName} req 
 * @param {secret} req 
 * @param {JSON} res
 */
function changeTrust(req, res, next) {
    var deferred = Q.defer();

    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var accountId = req.body.accountId;
    var accountNo = req.body.accountNo;
    var accountIfsc = req.body.accountIfsc;
    var assetName = req.body.assetName;
    var issrAccountId = configJS.issuerAccount;
    var accountSeed = req.body.secret;
    var body = {};
    body.id = accountId;
    body.first_name = firstName;
    body.last_name = lastName;
    body.name = firstName;
    body.domain = config.domainName;
    body.friendly_id = firstName + "_" + lastName;
    body.bank_account = accountNo;
    body.bank_ifsc = accountIfsc;
    var keyPairs = StellarSdk.Keypair.fromSecret(accountSeed);
    var friendlyId = firstName + "_" + lastName;
    // First, the receiving account must trust the asset
    server.loadAccount(keyPairs.publicKey())
        .then(function (receiver) {

            var transaction = new StellarSdk.TransactionBuilder(receiver)
                // The `changeTrust` operation creates (or alters) a trustline
                // The `limit` parameter below is optional
                .addOperation(StellarSdk.Operation.changeTrust({
                    asset: new StellarSdk.Asset(assetName, issrAccountId),
                    // limit: amountIssued
                }))
                .build();

            transaction.sign(keyPairs);
            server.submitTransaction(transaction);
            if (dbType === "postgres") {
                db.none('insert into accounts(id, first_name, last_name, name, domain, friendly_id, bank_account, bank_ifsc)' +
                    'values(${id}, ${first_name}, ${last_name}, ${name}, ${domain}, ${friendly_id}, ${bank_account}, ${bank_ifsc})',
                    body)
                    .then(function () {
                        deferred.resolve({
                            status: 'success',
                            message: 'Trustline created successfully.'
                        })
                    })
                    .catch(function (err) {
                        return next(err);
                    });
            } else if (dbType === "mysql") {
                var sql = "INSERT INTO internal_accounts (account_id, first_name, last_name, name, domain, friendly_id, account_no, account_ifsc) VALUES " +
                    "('" + accountId + "', '" + firstName + "', '" + lastName + "', '" + firstName + "', '" + config.domainName + "'" +
                    ", '" + friendlyId + "', '" + accountNo + "', '" + accountIfsc + "')";
                connection.query(sql, function (err, result) {
                    if (err) deferred.reject(err)
                    // console.log("1 record inserted");
                    deferred.resolve({
                        status: 'success',
                        message: 'Trustline created successfully.'
                    })
                });
            }

        })
        .catch(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
}

/**
 * @author:Akshay Misal
 * @link: POST /asset/issueAsset
 * @description:This function will issue asset
 * @param {assetName,receiverAccount,issueAsset} req 
 * @param {issuer} res 
 */
function issueAsset(req, res) {
    console.log("final issue asset function => ", req.body);
    var deferred = Q.defer();
    var issuerAccount = configJS.issuerAccount;
    var issuerSecret = configJS.issuerSecret;
    var assetName = req.body.assetName;
    var receiverAccount = req.body.receiverAccount;
    var issueAsset = req.body.issueAsset;
    var issuingKeys = StellarSdk.Keypair.fromSecret(issuerSecret);
    server.loadAccount(issuingKeys.publicKey())
        .then(function (issuer) {
            var transaction = new StellarSdk.TransactionBuilder(issuer)
                .addOperation(StellarSdk.Operation.payment({
                    destination: receiverAccount,
                    asset: new StellarSdk.Asset(assetName, issuingKeys.publicKey()),
                    amount: issueAsset
                }))
                .build();
            transaction.sign(issuingKeys);
            server.submitTransaction(transaction);
            deferred.resolve(issuer);
        }).catch(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
}  