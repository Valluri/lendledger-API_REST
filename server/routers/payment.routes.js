/**
 * @author Akshay Misal
 * @version 1.1.0
 * @since 09-Aug-2018
 */
var express = require('express');
var router = express.Router();
var Q = require('q');
var config = require('./../config/config.json');
var paymentService = require("./../services/payment.service");

router.post('/lend', lend);
router.post('/exchange', exchange);

module.exports = router;

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function lend(req, res) {
    paymentService.lend(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function exchange(req, res) {
    paymentService.exchange(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}