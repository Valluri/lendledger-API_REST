var fs = require('fs');
var http = require('http');
var https = require('https');
var config = require('./../config/config.json');
var configJS = require('./../config/config');

//Giri: Configuration for Logging
var morgan = require('morgan');
var winston = require('./../config/winston');

var credentials = {
    key: fs.readFileSync(process.env.CERT_PATH+config.key),
    cert: fs.readFileSync(process.env.CERT_PATH+config.cert),
    ca: fs.readFileSync(process.env.CERT_PATH+'csr.pem'),
};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = process.env.TLS_FLAG  // in testing mode set TLS_FLAG=0

var express = require('express');
var app = express();
var session = require('express-session');
var bodyParser = require('body-parser');
var http = require('http');
var swaggerUi = require('swagger-ui-express');

var expressJwt = require('express-jwt');
var cookieParser = require('cookie-parser');
var helmet = require('helmet');
var cors = require('cors');

app.use(cors());
app.use(helmet());
app.use(cookieParser());

app.use(session({ secret: config.secret, resave: false, saveUninitialized: true, maxAge: '60000' }));

// use JWT auth to secure the api
app.use('', expressJwt({ secret: config.secret }).unless({ path: ['/auth/','/account/create','/favicon.ico'] }));

// var server = http.createServer(app);

var mongo = require('mongodb').MongoClient;

app.get('/', function (req, res) {
    res.send('LendLedger');
})

//Path of swagger.json file in your app directory
var swaggerDocument = require('./../../doc/swagger.json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan('combined', { stream: winston.stream }));


app.use('/auth',require('./auth.routes'));
app.use('/account', require('./account.routes'));
app.use('/payment', require('./payment.routes'));
app.use('/asset', require('./asset.routes'));
app.use('/offer', require('./offer.routes'));

//Expose your swagger documentation through your express framework
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument)); 

var StellarSdk = require('stellar-sdk')
var stellarServer = new StellarSdk.Server(config.stellarServer);

// Get a message any time a payment occurs. Cursor is set to "now" to be notified
// of payments happening startinsg from when this script runs (as opposed to from
// the beginning of time).
var es = stellarServer.payments()
    .cursor('now')
    .stream({
        onmessage: function (message) {
            // console.log(message);
        }
    })

// error handler for logging using winston
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // add this line to include winston logging
    winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  
    // render the error page
    res.status(err.status || 500);
    // next();
    res.send('error');
  });

var server = https.createServer(credentials, app) ;
const port = process.env.PORT || config.port;
server.listen(port);   
console.log("Server listening on https://",port);

module.exports = server