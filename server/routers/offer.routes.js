/**
 * @author Akshay Misal
 * @version 1.1.0
 * @since 09-Aug-2018
 */
var express = require('express');
var router = express.Router();
var Q = require('q');
var config = require('./../config/config.json');
var offerService = require('./../services/offer.service');

router.post('/manageOffer', manageOffer);
router.get('/getOffer', getOffer);

module.exports = router;

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function manageOffer(req, res) {
    offerService.manageOffer(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function getOffer(req, res) {
    offerService.getOffer(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}