/**
 * @author Akshay Misal
 * @version 1.1.0
 * @since 09-Aug-2018
 */
var express = require('express');
var router = express.Router();
var Q = require('q');
var config = require('./../config/config.json');
var assetService = require('./../services/asset.service');

router.post('/changeTrust', changeTrust);
router.post('/issueAsset', issueAsset);

module.exports = router;

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function changeTrust(req, res) {
    assetService.changeTrust(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function issueAsset(req, res) {
    assetService.issueAsset(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}
