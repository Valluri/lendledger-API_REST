/**
 * @author Akshay Misal
 * @version 1.1.0
 * @since 09-Aug-2018
 */
var express = require('express');
var router = express.Router();
var Q = require('q');
var config = require('./../config/config.json');
var accService = require('./../services/account.service');

router.post('/create', create);
router.get('/getAccount', getAccountDetails);
router.get('/auditAccount', getAccountAudit);

module.exports = router;

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function create(req, res) {
    accService.create(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function getAccountDetails(req, res) {
    accService.getAccountDetails(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function getAccountAudit(req, res) {
    accService.getAccountAudit(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}