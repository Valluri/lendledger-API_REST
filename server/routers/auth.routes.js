/**
 * @author Akshay Misal
 * @version 1.1.0
 * @since 09-Aug-2018
 */
var express = require('express');
var router = express.Router();
var Q = require('q');
var config = require('./../config/config.json');
var authService = require('./../services/auth.service');

router.get('/', authenticate);

module.exports = router;

/**
 * @author Akshay Misal
 * @description This function will call the account-service.
 */
function authenticate(req, res) {
    authService.authenticate(req, res)
        .then(function (data) {
            res.send(data);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}